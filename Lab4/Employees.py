class Employee:

    def __init__(self, name=None, place_of_work=None, field=None):
        self.__name = name
        self.__place_of_work = place_of_work
        self._dash = lambda: print('-' * 50)
        self.__field = field
        self.task_list_obj = list()

    def get_Name(self):
        return self.__name

    def get_Place_of_work(self):
        return self.__place_of_work

    def get_Field(self):
        return self.__field

    def _add_new_employee(self, numbers_of_employee, Class, data, name_field):
        self._dash()
        for i in range(numbers_of_employee):
            name = input(f'Введите ФИО {i + 1}-го сотрудника: ')
            place_of_work = input(f'Введите место работы {i + 1}-го сотрудника: ')
            try:
                field = int(input(f'Введите {name_field}: '))
            except:
                print('Введите число:')
                field = int(input(f'Введите {name_field}: '))
            data.append(Class(name, place_of_work, field))

    def _show_all_employee(self, data, name_employee, name_field):
        self._dash()
        for i in range(len(data)):
            print(f'{i + 1} {name_employee}:')
            print('ФИО сотрудника:', data[i].get_Name())
            print('Место работы:', data[i].get_Place_of_work())
            print(f'{name_field.capitalize()} :  {data[i].get_Field()}\n')
        self._dash()

    def _sorted_employee(self, data):
        data.sort(key=lambda x: x.get_sort())

    def task(self, data, condition, name_employee, name_field):
        self.task_list_obj = list(filter(condition, data))
        if len(self.task_list_obj) < 1:
            print('Таких сотрудников нет')
        else:
            self._show_all_employee(self.task_list_obj, name_employee, name_field)


class Worker(Employee):

    def __init__(self, name=None, place_of_work=None, rank=None):
        super().__init__(name, place_of_work, rank)
        self._Data_worker = list()
        self.name_field = 'разряд'
        self.name_employee = 'работник'
        self.condition = lambda x: x.get_Rank() != 1 and x.get_Rank() != 3

    def get_Rank(self):
        return super().get_Field()

    def add_new_worker(self, numbers_of_worker=0):
        super()._add_new_employee(numbers_of_worker, Worker, self._Data_worker, self.name_field)
        print('Не отсортированный массив')
        self.show_all_workers()

    def sorted_worker(self):
        super()._sorted_employee(self._Data_worker)
        print('Отсортированный массив')
        self.show_all_workers()

    def get_sort(self):
        return super(Worker, self).get_Field()

    def show_all_workers(self):
        super()._show_all_employee(self._Data_worker, self.name_employee, self.name_field)

    def task_worker(self):
        print('''Задание 1.
Перечень рабочих, разряд которых не равен 1 и не равен 3: ''')

        super().task(self._Data_worker, self.condition, self.name_employee, self.name_field)

class Scientist(Employee):

    def __init__(self, name=None, place_of_work=None, num_of_publication=None):
        super().__init__(name, place_of_work, num_of_publication)
        self._Data_scientist = list()
        self.name_field = 'количество публикаций'
        self.name_employee = 'научный работник'

    def get_Num_of_publications(self):
        return super().get_Field()

    def add_new_scientist(self, numbers_of_worker=0):
        super()._add_new_employee(numbers_of_worker, Scientist, self._Data_scientist, self.name_field)
        print('Не отсортированный массив')
        self.show_all_scientists()

    def sorted_scientist(self):
        super()._sorted_employee(self._Data_scientist)
        print('Отсортированный массив')
        self.show_all_scientists()

    def get_sort(self):
        return super(Scientist, self).get_Name()

    def show_all_scientists(self):
        super()._show_all_employee(self._Data_scientist, self.name_employee, self.name_field)

    def task_scientists(self):
        print('''Задание 2.
Перечень научных работников, число публикаций которых больше 
среднего числа публикаций всех научных работников из массива : ''')

        average_publication =  int(sum([i.get_Num_of_publications()
                                        for i in self._Data_scientist] ) / len(self._Data_scientist))

        condition = lambda data: data.get_Num_of_publications() > average_publication
        super().task(self._Data_scientist, condition, self.name_employee, self.name_field)
