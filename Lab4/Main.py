from Employees import Employee, Worker, Scientist


class Main(Worker, Scientist, Employee):

    def add_worker(self):
        worker = Worker()

        try:
            numbers_of_workers = int(input('Введите количество рабочих: '))
            if numbers_of_workers > 0:
                worker.add_new_worker(numbers_of_workers)
                worker.sorted_worker()
                worker.task_worker()
            else:
                print('Число рабочих должно быть больше 0')
                Main().add_worker()
        except:
            print('Ошибка: Это не число. Введите число.')
            Main().add_worker()

    def add_scientist(self):
        scientist = Scientist()

        try:
            numbers_of_scientist = int(input('Введите количество научных работников: '))
            if numbers_of_scientist > 0:
                scientist.add_new_scientist(numbers_of_scientist)
                scientist.sorted_scientist()
                scientist.task_scientists()
            else:
                print('Число научных работников должно быть больше 0')
                Main().add_scientist()
        except:
            print('Ошибка: Это не число. Введите число.')
            Main().add_scientist()

if __name__ == '__main__':
    Main().add_worker()
    Main().add_scientist()
