import Worker

class Main(Worker):

    def __init__(self):
        self.numbers_of_workers = None

    def main(self):
        try:
            self.numbers_of_workers = int(input('Введите количество сотрудников: '))

            if self.numbers_of_workers > 0:

                self.add_new_worker(self.numbers_of_workers)
                # 3 ЛР
                print('Не отсортированный массив сотрудников')
                self.show_all_workers()

                print('Отсортированный по возрастанию разряда сотрудника массив')
                self.Data_worker.sort(key=lambda x: x.getRank(), reverse=True)
                print(self.show_all_workers())

            else:
                print('Число сотрудников должно быть больше 0')
                Main().main()

        except:
            print('Это не число. Введите число.')
            Main().main()

Main().main()



