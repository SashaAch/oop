class Worker:

    def __init__(self, name=None, rank=None, place_of_work=None):
        self.__place_of_work = place_of_work
        self.__rank = rank
        self.__name = name
        self.__rank_tuple, self.__name_list = tuple(), list()
        self.dash = lambda: print('-' * 50)
        self.Data_worker = list()

    def getName(self):
        return self.__name

    def getRank(self):
        return self.__rank

    def getPlace_of_work(self):
        return self.__place_of_work


    def add_new_worker(self, numbers_work=0):
        self.dash()
        for i in range(numbers_work):
            name = input(f'Введите ФИО {i + 1}-го сотрудника: ')

            try:
                rank = int(input(f'Введите разряд {i + 1}-го сотрудника: '))
            except:
                print('Введите число:')
                rank = int(input(f'Введите разряд {i + 1}-го сотрудника: '))

            place_of_work = input(f'Введите место работы {i + 1}-го сотрудника: ')
            self.Data_worker.append(Worker(name, rank, place_of_work))
            self.dash()

    def show_all_workers(self):
        data = []
        for id, i in enumerate(range(len(self.Data_worker))):
            print(f'{i + 1} сотрудник:')
            # data.append((self.Data_worker[i].getName(), self.Data_worker[i].getRank(),self.Data_worker[i].getPlace_of_work()))
            self.Data_worker[i].show()
            # print(id+1, data[i])
        del data
        self.dash()

    def show(self):
        print('ФИО работника:', self.getName())
        print('Разряд рабочего: ', self.getRank())
        print('Место работы:', self.getPlace_of_work())

    # 2 ЛР
    # 1 Задание
    def show_not_min_rank(self, data):
        print('1 задание. Вывод сотрудников с не минимальным разрядом.')

        self.__rank_tuple += (tuple(i.getRank() for i in data))

        if len(self.__rank_tuple) <= 1:
            print('Список пуст')
        else:
            for i in list(filter(lambda x: x.getRank() is not min(self.__rank_tuple), data)):
                i._show()

        self.dash()

    # 2 Задание
    def show_name_start_vpt(self, data):
        print('2 задание. Вывод сотрудников, имена которых начинаются с В, П или Т.')

        for i in data:
            if i.getName()[0] in ('В', 'П', 'Т'):
                self.__name_list.append(i)
                i._show()

        if len(self.__name_list) == 0:
            print('Нет имён начинающихся на В, П или Т')
